//
//  UIView+AutoLayoutDSL.mm
//  CAutoLayoutDSL
//
//  Created by lvv.me on 2022/5/18.
//

#import "UIView+AutoLayoutDSL.h"

@implementation UIView (AutoLayoutDSL)

- (CAutoLayoutDSL::CLayoutXAxisAnchor)leading {
    return CAutoLayoutDSL::CLayoutXAxisAnchor(self.leadingAnchor);
}

- (CAutoLayoutDSL::CLayoutXAxisAnchor)trailing {
    return CAutoLayoutDSL::CLayoutXAxisAnchor(self.trailingAnchor);
}

- (CAutoLayoutDSL::CLayoutYAxisAnchor)top {
    return CAutoLayoutDSL::CLayoutYAxisAnchor(self.topAnchor);
}

- (CAutoLayoutDSL::CLayoutXAxisAnchor)left {
    return CAutoLayoutDSL::CLayoutXAxisAnchor(self.leftAnchor);
}

- (CAutoLayoutDSL::CLayoutYAxisAnchor)bottom {
    return CAutoLayoutDSL::CLayoutYAxisAnchor(self.bottomAnchor);
}

- (CAutoLayoutDSL::CLayoutXAxisAnchor)right {
    return CAutoLayoutDSL::CLayoutXAxisAnchor(self.rightAnchor);
}

- (CAutoLayoutDSL::CLayoutXAxisAnchor)centerX {
    return CAutoLayoutDSL::CLayoutXAxisAnchor(self.centerXAnchor);
}

- (CAutoLayoutDSL::CLayoutYAxisAnchor)centerY {
    return CAutoLayoutDSL::CLayoutYAxisAnchor(self.centerYAnchor);
}

- (CAutoLayoutDSL::CLayoutDimension)width {
    return CAutoLayoutDSL::CLayoutDimension(self.widthAnchor);
}

- (CAutoLayoutDSL::CLayoutDimension)height {
    return CAutoLayoutDSL::CLayoutDimension(self.heightAnchor);
}

@end
