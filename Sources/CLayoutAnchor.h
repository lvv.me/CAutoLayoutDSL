//
//  CLayoutAnchor.h
//  CAutoLayoutDSL
//
//  Created by lvv.me on 2022/5/18.
//

#ifdef __cplusplus

#ifndef _CLayoutAnchor_h
#define _CLayoutAnchor_h

#import <TargetConditionals.h>

#if TARGET_OS_OSX
#import <AppKit/AppKit.h>
#define UIView NSView
#define UILayoutGuide NSLayoutGuide
#else
#import <UIKit/UIKit.h>
#endif

#import <initializer_list>
#import <algorithm>

namespace CAutoLayoutDSL {

template<typename T = NSLayoutAnchor>
class API_AVAILABLE(macos(10.11), ios(9.0), tvos(9.0)) CLayoutAnchor final {
public:
    CLayoutAnchor(T *layoutAnchor)
        : m_layoutAnchor(layoutAnchor)
        , m_multiplier(1)
        , m_constant(0) {
    }

    CLayoutAnchor() = delete;

    ~CLayoutAnchor() {
        m_layoutAnchor = nil;
    }

    template<typename _T = T>
    CLayoutAnchor & operator* (CGFloat m) {
        return *this;
    }

    template<typename _T = T>
    CLayoutAnchor & operator/ (CGFloat m) {
        return *this;
    }

    template<>
    CLayoutAnchor & operator* <NSLayoutDimension>(CGFloat m) {
        m_multiplier *= m;

        return *this;
    }

    template<>
    CLayoutAnchor & operator/ <NSLayoutDimension>(CGFloat m) {
        m_multiplier /= m;

        return *this;
    }

    CLayoutAnchor & operator+ (CGFloat c) {
        m_constant += c;

        return *this;
    }

    CLayoutAnchor & operator- (CGFloat c) {
        m_constant -= c;

        return *this;
    }

    template<typename _T = T>
    NSLayoutConstraint * operator<= (const CLayoutAnchor<T> &rhs) {
        return [m_layoutAnchor constraintLessThanOrEqualToAnchor:rhs.m_layoutAnchor constant:rhs.m_constant];
    }

    template<typename _T = T>
    NSLayoutConstraint * operator>= (const CLayoutAnchor<T> &rhs) {
        return [m_layoutAnchor constraintGreaterThanOrEqualToAnchor:rhs.m_layoutAnchor constant:rhs.m_constant];
    }

    template<typename _T = T>
    NSLayoutConstraint * operator== (const CLayoutAnchor<T> &rhs) {
        return [m_layoutAnchor constraintEqualToAnchor:rhs.m_layoutAnchor constant:rhs.m_constant];
    }

    template<>
    NSLayoutConstraint * operator<= <NSLayoutDimension>(const CLayoutAnchor<T> &rhs) {
        return [m_layoutAnchor constraintLessThanOrEqualToAnchor:rhs.m_layoutAnchor multiplier:rhs.m_multiplier constant:rhs.m_constant];
    }

    template<>
    NSLayoutConstraint * operator>= <NSLayoutDimension>(const CLayoutAnchor<T> &rhs) {
        return [m_layoutAnchor constraintGreaterThanOrEqualToAnchor:rhs.m_layoutAnchor multiplier:rhs.m_multiplier constant:rhs.m_constant];
    }

    template<>
    NSLayoutConstraint * operator== <NSLayoutDimension>(const CLayoutAnchor<T> &rhs) {
        return [m_layoutAnchor constraintEqualToAnchor:rhs.m_layoutAnchor multiplier:rhs.m_multiplier constant:rhs.m_constant];
    }

    template<typename _T = T>
    NSLayoutConstraint * operator<= (CGFloat c) {
        return *this;
    }

    template<>
    NSLayoutConstraint * operator<= <NSLayoutDimension>(CGFloat c) {
        m_constant = c;

        return [m_layoutAnchor constraintLessThanOrEqualToConstant:c];
    }

    template<typename _T = T>
    NSLayoutConstraint * operator>= (CGFloat c) {
        return *this;
    }

    template<>
    NSLayoutConstraint * operator>= <NSLayoutDimension>(CGFloat c) {
        m_constant = c;

        return [m_layoutAnchor constraintGreaterThanOrEqualToConstant:c];
    }

    template<typename _T = T>
    NSLayoutConstraint * operator== (CGFloat c) {
        return *this;
    }

    template<>
    NSLayoutConstraint * operator== <NSLayoutDimension>(CGFloat c) {
        m_constant = c;

        return [m_layoutAnchor constraintEqualToConstant:c];
    }

private:
    T *m_layoutAnchor;
    CGFloat m_multiplier;
    CGFloat m_constant;
};

typedef CLayoutAnchor<NSLayoutXAxisAnchor> CLayoutXAxisAnchor;
typedef CLayoutAnchor<NSLayoutYAxisAnchor> CLayoutYAxisAnchor;
typedef CLayoutAnchor<NSLayoutDimension> CLayoutDimension;

template<typename T = NSLayoutConstraint>
static inline void activateConstraints(std::initializer_list<T *> list) API_AVAILABLE(macos(10.11), ios(9.0), tvos(9.0)) {
    std::for_each(list.begin(), list.end(), [](auto item){
        item.active = YES;
    });
}

}

#endif /* _CLayoutAnchor_h */

#endif /* __cplusplus */
